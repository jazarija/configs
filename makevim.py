#!/usr/bin/python
# Author : Jernej Azarija

# Description : In order to use this you need the associated vimrc file! When Ctrl+W is pressed
# the script tries to guess what kind of program are you working on and invoking the right program for it
# Currently supports latex, C, C++, xcode, make projects and java.

# Date : Fri Nov 24 15:32:07 CET 2006

# In order to make this work, set makeprg="python makevim.py %"
# TODO Reimplement this code as a dict of function ptrs

import sys,os

for f in os.listdir(os.getcwd()) :
	if f == 'Makefile' :
		os.system('make')
		sys.exit(0)
	elif f.find('xcodeproj') != -1 : 
		os.system('xcodebuild')
		sys.exit(0)					   

else :
	flags = '-Wall -Wextra -lm -lgmp -W -O0 -ggdb -lpthread '
	extension = sys.argv[1].split('.')
	extension = sys.argv[1].split('.')[-1]

	# The extensions are listed by their frequence.
	if extension == 'c' :
		os.system('gcc ' + flags  + sys.argv[1])
	elif (extension == 'cpp' or extension == 'c++') :
		os.system('g++ ' + flags  + sys.argv[1])
	elif	extension == 'py' :
		os.system('/usr/bin/python ' + sys.argv[1])					   
	elif extension == 'pas' :
		os.system('fpc ' + sys.argv[1])					   
	elif extension == 'java' :
		os.system('javac -Xlint ' + sys.argv[1])					   
	elif extension == 'tex' :
		os.system('pdflatex ' + sys.argv[1])					   
							 	
