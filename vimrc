set hlsearch
set nocompatible    " use vim defaults
set ls=2            " allways show status line
set tabstop=4       " numbers of spaces of tab character
set shiftwidth=4    " numbers of spaces to (auto)indent
set scrolloff=3     " keep 3 lines when scrolling
set cindent         " cindent
set smartindent     " smart indent
set autoindent      " always set autoindenting on
set showcmd         " display incomplete commands
set incsearch       " do incremental searching
set ruler           " show the cursor position all the time
set nobackup        " do not keep a backup file
set number          " show line numbers
set ignorecase      " ignore case when searching 
set ttyfast         " smoother changes
set modeline        " last lines in document sets vim mode
set modelines=3     " number lines checked for modelines
set nostartofline   " don't jump to first character when paging
set t_Co=256        " support for 256 bit colors 

colors inkpot       
syntax on

set smartindent
set tabstop=4
set shiftwidth=4
set expandtab
" Some mappings for multiple tabs support

map <C-Y> :tabprevious<CR><CR>
" Next tab 
map <C-X> :tabnext<CR><CR>
" New tab 
map <C-N> :tabnew<CR><CR>
" Close (forced) of the current tab
map <C-C> :tabclose!<CR><CR>

map <C-W> :set makeprg=makevim.py\ %<BAR>make<CR>

" Linux kernel coding stle
""set ts=8
""set textwidth=80

let g:tex_flavor='latex'

" Support for sage
autocmd BufRead,BufNewFile *.sage,*.pyx,*.spyx set filetype=python
autocmd Filetype python set tabstop=4|set shiftwidth=4|set expandtab
autocmd FileType python set makeprg=sage\ -b\ &&\ sage\ -t\ %

